SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK


DECLARE
	 ELEMENT_CONT number;

	 V_SERV_CODE ESB_SERVICE.CODE%TYPE;
     V_SERV_NAME ESB_SERVICE.NAME%TYPE;
     V_CAP_OP_CODE ESB_CAPABILITY.CODE%TYPE;
     V_CAP_OP_NAME ESB_CAPABILITY.NAME%TYPE;

    PROCEDURE CREATE_CAPABILITY IS
	BEGIN
	-- ESB_CAPABILITY ----------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_CONT	FROM ESB_CAPABILITY	WHERE NAME = V_CAP_OP_NAME AND SERVICE_ID = (SELECT ID FROM ESB_SERVICE WHERE CODE = V_SERV_CODE AND NAME = V_SERV_NAME);
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_CAPABILITY (ID,SERVICE_ID,CODE,NAME,RCD_STATUS)  VALUES (ESB_CAPABILITY_SEQ.NEXTVAL,( SELECT ID FROM ESB_SERVICE WHERE CODE = V_SERV_CODE AND NAME = V_SERV_NAME),V_CAP_OP_CODE,V_CAP_OP_NAME,'1');
		ELSE
			UPDATE ESB_CAPABILITY SET RCD_STATUS = 1 WHERE NAME = V_CAP_OP_NAME AND SERVICE_ID = (SELECT ID FROM ESB_SERVICE WHERE CODE = V_SERV_CODE AND NAME = V_SERV_NAME);
			DBMS_OUTPUT.PUT_LINE('ESB_CAPABILITY             '||V_CAP_OP_CODE||' ya existe');
		END IF;

	-- ESB_CAPABILITY_CHECK ----------------------------------------------------------------------------------------------------------------------------

		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_CAPABILITY_CHECK WHERE CAPABILITY_ID=( SELECT ID FROM ESB_CAPABILITY WHERE NAME = V_CAP_OP_NAME  AND SERVICE_ID = (SELECT ID FROM ESB_SERVICE WHERE CODE = V_SERV_CODE AND NAME = V_SERV_NAME) );
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_CAPABILITY_CHECK (ID,CAPABILITY_ID,CHECK_INDICATOR,RCD_STATUS)
			VALUES (ESB_CAPABILITY_CHECK_SEQ.NEXTVAL,( SELECT ID FROM ESB_CAPABILITY WHERE CODE = V_CAP_OP_CODE),'0','1');
		ELSE
			UPDATE ESB_CAPABILITY_CHECK SET RCD_STATUS = 1 WHERE CAPABILITY_ID=( SELECT ID FROM ESB_CAPABILITY WHERE NAME = V_CAP_OP_NAME  AND SERVICE_ID = (SELECT ID FROM ESB_SERVICE WHERE CODE = V_SERV_CODE AND NAME = V_SERV_NAME) );
			DBMS_OUTPUT.PUT_LINE('ESB_CAPABILITY_CHECK       '||V_CAP_OP_CODE||' ya existe');
		END IF;

	-- ESB_PARAMETER  -------------------------------------------------------------------------------------------------------------------------------

		SELECT COUNT(*) INTO ELEMENT_CONT	FROM ESB_PARAMETER	WHERE KEY='Logger.error.IsEnabled.'||V_SERV_CODE||'.'||V_SERV_NAME||'.'||V_CAP_OP_NAME;
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_PARAMETER (ID, KEY,VALUE,RCD_STATUS) VALUES (ESB_PARAMETER_SEQ.NEXTVAL, 'Logger.error.IsEnabled.'||V_SERV_CODE||'.'||V_SERV_NAME||'.'||V_CAP_OP_NAME,'1','1');
		ELSE
			UPDATE ESB_PARAMETER SET RCD_STATUS = 1 WHERE KEY='Logger.error.IsEnabled.'||V_SERV_CODE||'.'||V_SERV_NAME||'.'||V_CAP_OP_NAME;
			DBMS_OUTPUT.PUT_LINE('ESB_PARAMETER Logger.error '||V_CAP_OP_NAME||' ya existe');
		END IF;

		SELECT COUNT(*) INTO ELEMENT_CONT	FROM ESB_PARAMETER	WHERE KEY='Logger.log.IsEnabled.'||V_SERV_CODE||'.'||V_SERV_NAME||'.'||V_CAP_OP_NAME;
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_PARAMETER (ID, KEY,VALUE,RCD_STATUS) VALUES (ESB_PARAMETER_SEQ.NEXTVAL, 'Logger.log.IsEnabled.'||V_SERV_CODE||'.'||V_SERV_NAME||'.'||V_CAP_OP_NAME,'1','1');
		ELSE
			UPDATE ESB_PARAMETER SET RCD_STATUS = 1 WHERE KEY='Logger.log.IsEnabled.'||V_SERV_CODE||'.'||V_SERV_NAME||'.'||V_CAP_OP_NAME;
			DBMS_OUTPUT.PUT_LINE('ESB_PARAMETER Logger.log '||V_CAP_OP_NAME||' ya existe');
		END IF;

	END CREATE_CAPABILITY;

	PROCEDURE CREATE_SERVICE IS
	BEGIN
	-- ESB_SERVICE ----------------------------------------------------------------------------------------------------------------------------------
 		SELECT COUNT(*) INTO ELEMENT_CONT FROM ESB_SERVICE WHERE CODE=V_SERV_CODE AND NAME = V_SERV_NAME;
		IF ELEMENT_CONT=0 THEN
			INSERT INTO ESB_SERVICE (ID,CODE,NAME,RCD_STATUS) VALUES (ESB_SERVICE_SEQ.NEXTVAL,V_SERV_CODE,V_SERV_NAME,'1');
		ELSE
			UPDATE ESB_SERVICE SET RCD_STATUS = 1 WHERE CODE=V_SERV_CODE AND NAME = V_SERV_NAME;
			DBMS_OUTPUT.PUT_LINE('ESB_SERVICE '||V_SERV_CODE||' ya existe');
		END IF;
	END CREATE_SERVICE;

BEGIN

	 V_SERV_CODE :='${service.code}';
	 V_SERV_NAME :='${service.name}';

	 CREATE_SERVICE;


    <#list operations as operation>
        V_CAP_OP_CODE:= '${operation.code}';
        V_CAP_OP_NAME:= '${operation.name}';
        CREATE_CAPABILITY;
    </#list>

	--Can append more instruccion if the Service need it

END;
/
COMMIT;