SET ECHO ON
SET DEFINE OFF
SET SERVEROUTPUT ON
WHENEVER SQLERROR CONTINUE ROLLBACK

DECLARE
	ELEMENT_COUNT number;

	V_SYSTEM_CODE ESB_SYSTEM.CODE%TYPE;
	V_SYSTEM_NAME ESB_SYSTEM.NAME%TYPE;
	V_SYSTEM_DESC ESB_SYSTEM.DESCRIPTION%TYPE;

	V_API_CODE ESB_SYSTEM_API.CODE%TYPE;
	V_API_NAME ESB_SYSTEM_API.NAME%TYPE;
	V_API_DESC ESB_SYSTEM_API.DESCRIPTION%TYPE;

	V_API_OP_VER ESB_SYSTEM_API_OPERATION.VERSION%TYPE;
	V_API_OP_NAME ESB_SYSTEM_API_OPERATION.NAME%TYPE;
	V_API_OP_DESC ESB_SYSTEM_API_OPERATION.DESCRIPTION%TYPE;

	V_ESB_ENDPOINT_TRANSPORT ESB_ENDPOINT_TRANSPORT.NAME%TYPE;
	V_ESB_CONFIG ESB_CONFIG.NAME%TYPE;
	V_INSTANCE ESB_ENDPOINT.INSTANCE%TYPE;

<#if adapter.endpoints.uat.configuration.values.SecondsToLive??>
    V_SecondsToLive_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
</#if>
    <#if adapter.endpoints.uat.configuration.values.url??>
      V_URL_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
    </#if>
    <#if adapter.endpoints.uat.configuration.values.soapAction??>
      V_soapAction_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
    </#if>
    <#if adapter.endpoints.uat.configuration.values.user??>
       V_USER_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
    </#if>
    <#if adapter.endpoints.uat.configuration.values.password??>
        V_PASSWORD_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
    </#if>

<#if adapter.endpoints.uat.configuration.values.IP??>
    V_IP_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
</#if>

<#if adapter.endpoints.uat.configuration.values.Port??>
	V_Port_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
</#if>


<#if adapter.endpoints.uat.configuration.values.UserAgent??>
	V_UserAgent_VALUE ESB_CONFIG_LIST.VALUE%TYPE;
</#if>

	PROCEDURE CREATE_OPER_API IS
	BEGIN

	-- ESB_SYSTEM_API_OPERATION ------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_SYSTEM_API_OPERATION WHERE SYSTEM_API_ID= ( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE AND SYSTEM_ID = (SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE)) AND VERSION=V_API_OP_VER AND NAME=V_API_OP_NAME;
		IF ELEMENT_COUNT=0 THEN
			INSERT INTO ESB_SYSTEM_API_OPERATION (ID,SYSTEM_API_ID,VERSION,NAME,DESCRIPTION,RCD_STATUS)
			VALUES (ESB_SYSTEM_API_OPERATION_SEQ.NEXTVAL,( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE AND SYSTEM_ID = (SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE)),V_API_OP_VER,V_API_OP_NAME,V_API_OP_DESC,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_SYSTEM_API_OPERATION '|| V_API_OP_NAME || ' de API ' ||  V_SYSTEM_CODE ||' ya existe');
		END IF;

	-- ESB_CONFIG -------------------------------------------------------------------------------------------------------------------------------------------
		V_ESB_CONFIG := V_API_CODE || '_' || V_API_OP_NAME || '_' || V_ESB_ENDPOINT_TRANSPORT;
		SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG;
		IF ELEMENT_COUNT=0 THEN

			INSERT INTO ESB_CONFIG (ID,NAME,RCD_STATUS) VALUES (ESB_CONFIG_SEQ.NEXTVAL,V_ESB_CONFIG,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_CONFIG '|| V_ESB_CONFIG ||' ya existe ');
		END IF;

	-- ESB_ENDPOINT -----------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_ENDPOINT WHERE  OPERATION_ID = ( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = V_API_OP_NAME AND SYSTEM_API_ID = ( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE))
															AND    TRANSPORT_ID = ( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = V_ESB_ENDPOINT_TRANSPORT);
	    IF ELEMENT_COUNT=0 THEN
			INSERT INTO ESB_ENDPOINT (OPERATION_ID,TRANSPORT_ID,INSTANCE,CONFIG_ID,RCD_STATUS)
				VALUES (
					( SELECT ID FROM ESB_SYSTEM_API_OPERATION WHERE NAME = V_API_OP_NAME AND SYSTEM_API_ID = ( SELECT ID FROM ESB_SYSTEM_API WHERE CODE = V_API_CODE)),
					( SELECT ID FROM ESB_ENDPOINT_TRANSPORT WHERE NAME = V_ESB_ENDPOINT_TRANSPORT),
					V_INSTANCE,
					( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),
					'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_ENDPOINT para operation '|| V_API_OP_NAME || ' de API ' || V_API_CODE  ||' ya existe configuracion');
		END IF;

	-- ESB_CONFIG_LIST ---------------------------------------------------------------------------------------------------------------------------------------

        <#if adapter.endpoints.uat.configuration.values.url??>
          SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
          																AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url');
          		IF ELEMENT_COUNT=0 THEN
          			INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
          				VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'url'),V_URL_VALUE,'1');
          		ELSE
          			DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST URL para '|| V_ESB_CONFIG || ' ya existe');
          		END IF;
        </#if>

        <#if adapter.endpoints.uat.configuration.values.soapAction??>
          SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
          																AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction');
            IF ( ELEMENT_COUNT=0 AND V_soapAction_VALUE IS NOT NULL) THEN
                INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                    VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'soapAction'),V_soapAction_VALUE,'1');
            ELSE
                DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST soapAction para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
            END IF;
        </#if>

		<#if adapter.endpoints.uat.configuration.values.user??>
            SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
            																AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'user');
            IF (ELEMENT_COUNT=0 AND V_USER_VALUE IS NOT NULL) THEN
                INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                    VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'user'),V_USER_VALUE,'1');
            ELSE
                DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST user para '|| V_ESB_CONFIG || ' ya existe');
            END IF;
        </#if>

        <#if adapter.endpoints.uat.configuration.values.password??>
           SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
                                                                        AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'password');
                IF ( ELEMENT_COUNT=0 AND V_PASSWORD_VALUE IS NOT NULL) THEN
                    INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                        VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'password'),V_PASSWORD_VALUE,'1');
                ELSE
                    DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST password para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
                END IF;
        </#if>

        <#if adapter.endpoints.uat.configuration.values.IP??>
           SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
                                                                        AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'IP');
                IF ( ELEMENT_COUNT=0 AND V_IP_VALUE IS NOT NULL) THEN
                    INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                        VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'IP'),V_IP_VALUE,'1');
                ELSE
                    DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST IP para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
                END IF;
        </#if>

        <#if adapter.endpoints.uat.configuration.values.Port??>
           SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
                                                                        AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'Port');
                IF ( ELEMENT_COUNT=0 AND V_Port_VALUE IS NOT NULL) THEN
                    INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                        VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'Port'),V_Port_VALUE,'1');
                ELSE
                    DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST Port para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
                END IF;
        </#if>

        <#if adapter.endpoints.uat.configuration.values.UserAgent??>
           SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
                                                                        AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'UserAgent');
                IF ( ELEMENT_COUNT=0 AND V_UserAgent_VALUE IS NOT NULL) THEN
                    INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                        VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'UserAgent'),V_UserAgent_VALUE,'1');
                ELSE
                    DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST UserAgent para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
                END IF;
        </#if>

         <#if adapter.endpoints.uat.configuration.values.SecondsToLive??>
           SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_CONFIG_LIST WHERE CONFIG_ID = ( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG)
                                                                        AND PROPERTY_ID = ( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'SecondsToLive');
                IF ( ELEMENT_COUNT=0 AND V_SecondsToLive_VALUE IS NOT NULL) THEN
                    INSERT INTO ESB_CONFIG_LIST (CONFIG_ID,PROPERTY_ID,VALUE,RCD_STATUS)
                        VALUES (( SELECT ID FROM ESB_CONFIG WHERE NAME = V_ESB_CONFIG),( SELECT ID FROM ESB_CONFIG_PROPERTY WHERE NAME = 'SecondsToLive'),V_SecondsToLive_VALUE,'1');
                ELSE
                    DBMS_OUTPUT.PUT_LINE('ESB_CONFIG_LIST SecondsToLive para '|| V_ESB_CONFIG  ||' ya existe o no es requerido');
                END IF;
        </#if>


	END CREATE_OPER_API;

	PROCEDURE CREATE_SYSTEM IS
	BEGIN
	-- ESB_SYSTEM ------------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_SYSTEM WHERE CODE=V_SYSTEM_CODE;
		IF ELEMENT_COUNT=0 THEN
			INSERT INTO ESB_SYSTEM (ID,CODE,NAME,DESCRIPTION,RCD_STATUS) VALUES (ESB_SYSTEM_SEQ.NEXTVAL,V_SYSTEM_CODE,V_SYSTEM_NAME,V_SYSTEM_DESC,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_SYSTEM '||V_SYSTEM_CODE||' ya existe');
		END IF;
	END CREATE_SYSTEM;

	PROCEDURE CREATE_API IS
	BEGIN
	-- ESB_SYSTEM_API ------------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(*) INTO ELEMENT_COUNT FROM ESB_SYSTEM_API WHERE CODE=V_API_CODE AND SYSTEM_ID = (SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE);
		IF ELEMENT_COUNT=0 THEN
			INSERT INTO ESB_SYSTEM_API (ID,SYSTEM_ID,CODE,NAME,DESCRIPTION,RCD_STATUS) 	VALUES (ESB_SYSTEM_API_SEQ.NEXTVAL,( SELECT ID FROM ESB_SYSTEM WHERE CODE = V_SYSTEM_CODE),V_API_CODE,V_API_NAME,V_API_DESC ,'1');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ESB_SYSTEM_API '||V_SYSTEM_CODE||' ya existe');
		END IF;
	END CREATE_API;

BEGIN

	V_SYSTEM_CODE := '${adapter.provider.code}';
	V_SYSTEM_NAME := '${adapter.provider.name}';
	V_SYSTEM_DESC := '${adapter.provider.description}';

	CREATE_SYSTEM;

	V_API_CODE    := '${adapter.api.code}';
	V_API_NAME    := '${adapter.api.name}';
	V_API_DESC    := '${adapter.api.description}';

	CREATE_API;

		V_API_OP_VER  := '${adapter.operation.version}';
		V_API_OP_NAME := '${adapter.operation.name}';
		V_API_OP_DESC := '${adapter.operation.description}';
		V_ESB_ENDPOINT_TRANSPORT := '${adapter.endpoints.uat.transport}';
		-- Solo completar en caso de usar instacia de transporte  en caso de no requerir asignar con := '';
		V_INSTANCE := '${adapter.endpoints.uat.instance}';
		<#if adapter.endpoints.uat.configuration.values.url??>
          V_URL_VALUE := '${adapter.endpoints.uat.configuration.values.url}';
        </#if>
		<#if adapter.endpoints.uat.configuration.values.soapAction??>
          V_soapAction_VALUE := '${adapter.endpoints.uat.configuration.values.soapAction}';
        </#if>
        <#if adapter.endpoints.uat.configuration.values.user??>
            V_USER_VALUE := '${adapter.endpoints.uat.configuration.values.user}';
        </#if>
        <#if adapter.endpoints.uat.configuration.values.password??>
            V_PASSWORD_VALUE := '${adapter.endpoints.uat.configuration.values.password}';
        </#if>
        <#if adapter.endpoints.uat.configuration.values.IP??>
            V_IP_VALUE := '${adapter.endpoints.uat.configuration.values.IP}';
        </#if>
        <#if adapter.endpoints.uat.configuration.values.Port??>
            V_Port_VALUE := '${adapter.endpoints.uat.configuration.values.Port}';
        </#if>
        <#if adapter.endpoints.uat.configuration.values.UserAgent??>
            V_UserAgent_VALUE := '${adapter.endpoints.uat.configuration.values.UserAgent}';
        </#if>
<#if adapter.endpoints.uat.configuration.values.SecondsToLive??>
    V_SecondsToLive_VALUE := '${adapter.endpoints.uat.configuration.values.SecondsToLive}';
</#if>
		CREATE_OPER_API;

END;
/
COMMIT;