package cl.entel.tde.soa.integration.model.Configuration;

public class ConfigurationType {
    public static String DB_CONNECTION_STRING = "";

    public static String DB_DRIVER_CLASS = "";

    public static String DB_USERNMAE = "";

    public static String DB_PASSWORD = "";

    public static String DB_IS_XA_TRANSACTIONAL = "";

    public static String JMS_QUEUE_NAME = "";

    public static String JMS_QUEUE_JNDI = "";

    public static String JMS_CONNECTION_FACTORY_NAME = "";

    public static String JMS_CONNECTION_FACTORY_JNDI = "";

    public static String JMS_IS_XA_TRANSACTIONAL = "";

    public static String JMS_PERSISTENCE = "";

    public static String JMS_DELIVERY_RETRY = "";

    public static String JMS_DELIVERY_RETRY_DELAY = "";

}
