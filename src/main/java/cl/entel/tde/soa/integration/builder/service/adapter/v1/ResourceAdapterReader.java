package cl.entel.tde.soa.integration.builder.service.adapter.v1;


import cl.entel.tde.soa.integration.domain.Target;
import cl.entel.tde.soa.integration.xml.xpath.XPathExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import javax.xml.xpath.XPathConstants;


public class ResourceAdapterReader {

    Logger logger = LoggerFactory.getLogger(ResourceAdapterReader.class);


    public Target read(Document document){
        String tarProvider = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[translate(@varName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TARGETPROVIDER']/expr/xqueryText/text()", XPathConstants.STRING);
        String tarApi = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[translate(@varName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TARGETAPI']/expr/xqueryText/text()", XPathConstants.STRING);
        String tarOperation = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[translate(@varName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TARGETOPERATION']/expr/xqueryText/text()", XPathConstants.STRING);
        String tarVersion = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[translate(@varName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')='TARGETVERSION']/expr/xqueryText/text()", XPathConstants.STRING);
        String destination = (String)XPathExecutor.execute(document, "//route-node/actions/route/service/@ref", XPathConstants.STRING);
        Target target = new Target(tarProvider.replace("'", ""),tarApi.replace("'", ""),tarOperation.replace("'", ""),tarVersion.replace("'", ""), destination);
        return target;
    }

    public Target read(String filepath){
        try{
            Document document = XPathExecutor.getDocumentBuilder().parse(filepath);
            return this.read(document);
        } catch (Exception e ){
            logger.error("Error parse file: " + filepath);
        }
        return new Target("", "", "", "", "");
    }


}
