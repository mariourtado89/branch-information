package cl.entel.tde.soa.integration.domain;

import cl.entel.tde.soa.integration.model.Configuration.Configuration;

public class Endpoint {

    private String environment;

    private Configuration configuration;

    private String transport;

    private String instance;

    public Endpoint() {
    }

    public Endpoint(String environment, Configuration configuration, String transport) {
        this.environment = environment;
        this.configuration = configuration;
        this.transport = transport;
    }

    public Endpoint(String environment, Configuration configuration, String transport, String instance) {
        this.environment = environment;
        this.configuration = configuration;
        this.transport = transport;
        this.instance = instance;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        if (transport != null){
            this.transport = transport;
        } else {
            this.transport="";
        }
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        if (instance != null){
            this.instance = instance;
        } else {
            this.instance="";
        }
    }
}
