package cl.entel.tde.soa.integration.model.project;

import cl.entel.tde.soa.integration.domain.ContextResources;
import cl.entel.tde.soa.integration.domain.Service;
import cl.entel.tde.soa.integration.domain.ServiceOperation;
import cl.entel.tde.soa.integration.model.esb.ResourceBus;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class ServiceProject {

    private Map<String,Service> services;


    public ServiceProject() {
        this.services = new HashMap<String, Service>();
    }

    public Service parse(String filepath){
        if (!filepath.contains("/OH/")){
            return null;
        }
        String project = filepath.split("/")[0];
        String operation = filepath.split("/OH/")[1].split("/")[0];
        Service service = services.get(project);
        if(service == null){
            service = this.parseService(filepath);
        }
        ServiceOperation serviceOperation;
        if(service.getOperations().get(operation) == null){
            serviceOperation = this.parseServiceOperation(service, filepath);
            service.addOperation(serviceOperation);
        } else {
            serviceOperation = service.getOperations().get(operation);
        }

        this.parseServiceOperationResource(serviceOperation, filepath);
        this.services.put(project, service);
        return service;
    }

    private Service parseService(String filepath){
        String project = filepath.split("/")[0];
        String serviceName = project.split("_")[1];
        String serviceVersion = project.split("_")[2];
        Service service = new Service("", serviceName, "", serviceVersion, "");
        ServiceOperation operation = this.parseServiceOperation(service, filepath);
        service.addOperation(operation);
        return service;
    }

    private ServiceOperation parseServiceOperation(Service service, String filepath){
        String operation = filepath.split("/OH/")[1].split("/")[0];
        ServiceOperation serviceOperation = new ServiceOperation(operation, "");
        //serviceOperation = this.parseServiceOperationResource(serviceOperation, filepath);
        return serviceOperation;
    }

    private ServiceOperation parseServiceOperationResource(ServiceOperation operation, String filepath){
        ResourceBus pipeline = ContextResources.get(filepath);
        operation.getResources().add(pipeline);
        return operation;
    }

    public Map<String, Service> getServices() {
        return services;
    }

    public void setServices(Map<String, Service> services) {
        this.services = services;
    }
}
