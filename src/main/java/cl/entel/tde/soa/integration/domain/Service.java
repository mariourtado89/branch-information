package cl.entel.tde.soa.integration.domain;

import java.util.HashMap;
import java.util.Map;

public class Service extends ServiceProvider {

    private Map<String, ServiceOperation> operations;

    private String domain;

    public Service(){
        super();
        this.operations = new HashMap<String, ServiceOperation>();
    }
    public Service(String code, String name, String description, String version, String domain) {
        super(code, name, description, version);
        this.domain = domain;
        this.operations = new HashMap<String, ServiceOperation>();
    }

    public Service(String name, String version, String operation){
        super("", name, "", version);
        this.domain="";
        this.operations = new HashMap<String, ServiceOperation>();
        ServiceOperation oper = new ServiceOperation(operation, "");
        this.operations.put(operation, oper);

    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Map<String, ServiceOperation> getOperations() {
        return operations;
    }

    public void setOperations(Map<String, ServiceOperation> operations) {
        this.operations = operations;
    }

    public void addOperation(ServiceOperation operation){
        this.operations.put(operation.getName(),operation);
    }
}
