package cl.entel.tde.soa.integration.model.builder;

import cl.entel.tde.soa.integration.model.esb.BusinessService;
import cl.entel.tde.soa.integration.xml.xpath.XPathExecutor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;

@Component
public class BusinessServiceBuilder {

    public BusinessService build(String filePath){
        try{

            Document document = XPathExecutor.getDocumentBuilder().parse(filePath);
            String name = filePath.split("/")[filePath.split("/").length-1];
            String uri = (String)XPathExecutor.execute(document, "/businessServiceEntry/endpointConfig/URI/value", XPathConstants.STRING);
            String transport = (String)XPathExecutor.execute(document, "/businessServiceEntry/endpointConfig/provider-id", XPathConstants.STRING);
            BusinessService resource = new BusinessService(filePath, name, uri, transport);
            return resource;
        } catch (Exception e ){
            e.printStackTrace();
        }
        return null;
    }
}
