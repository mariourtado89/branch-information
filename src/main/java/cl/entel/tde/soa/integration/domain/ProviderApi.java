package cl.entel.tde.soa.integration.domain;

import java.util.ArrayList;
import java.util.List;

public class ProviderApi {

    private String code;

    private String name;

    private String description;

    private List<ProviderApiOperation> operations;

    public ProviderApi(String code, String name, String description) {
        this.operations = new ArrayList<ProviderApiOperation>();
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProviderApiOperation> getOperations() {
        return operations;
    }

    public void setOperations(List<ProviderApiOperation> operations) {
        this.operations = operations;
    }
}
