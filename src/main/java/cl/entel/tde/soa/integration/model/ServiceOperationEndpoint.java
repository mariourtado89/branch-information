package cl.entel.tde.soa.integration.model;

import cl.entel.tde.soa.integration.domain.Service;
import cl.entel.tde.soa.integration.model.esb.ProxyService;
import cl.entel.tde.soa.integration.model.esb.ResourceBus;

import java.util.ArrayList;
import java.util.List;

public class ServiceOperationEndpoint {

    private Service services;

    private List<ResourceBus> proxys;

    public ServiceOperationEndpoint() {
        this.proxys = new ArrayList<ResourceBus>();
    }
    public ServiceOperationEndpoint(Service services) {
        this.services = services;
        this.proxys = new ArrayList<ResourceBus>();
    }

    public Service getServices() {
        return services;
    }

    public void setServices(Service services) {
        this.services = services;
    }

    public List<ResourceBus> getProxys() {
        return proxys;
    }

    public void setProxys(List<ResourceBus> proxys) {
        this.proxys = proxys;
    }
}
