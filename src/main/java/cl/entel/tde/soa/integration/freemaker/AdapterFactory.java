package cl.entel.tde.soa.integration.freemaker;

import cl.entel.tde.soa.integration.domain.Adapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AdapterFactory {

    @Value("${app.eusb.report.target.sql}")
    private String targetDirectory;

    @Autowired
    private Factory factory;

    public AdapterFactory() {
    }

    public void build(List<Adapter> adapters){
        adapters.forEach(x-> {
            this.generate(x);
        });
    }

    public void generate(Adapter adapter){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("adapter", adapter);
            factory.generate("freemaker/template", "adapter", targetDirectory+adapter.getTarget().getTargetApi()+ "_" + adapter.getTarget().getTargetOperation()+".sql", map);
        }catch (Exception e ){
            e.printStackTrace();
        }

    }
}
