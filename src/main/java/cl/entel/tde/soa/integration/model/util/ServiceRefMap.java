package cl.entel.tde.soa.integration.model.util;

import java.util.HashMap;
import java.util.Map;

public class ServiceRefMap {

    private static Map<String, String> map = null;

    private static  synchronized void init(){
        map = new HashMap<String, String>();
        map.put("BusinessServiceRef", "bix");
        map.put("ProxyRef", "proxy");
        map.put("PipelineRef", "pipeline");
    }

    public static synchronized String get(String key){
        if ( map == null){
            init();
        }
        return map.get(key);
    }
}
