package cl.entel.tde.soa.integration.model.esb;

import java.util.ArrayList;
import java.util.List;

public class ResourceBusGeneric {

    protected String path;
    protected String name;
    protected List<ResourceRef> references;
    protected List<ResourceBus> resourceRef;

    public ResourceBusGeneric() {
        this.references = new ArrayList<ResourceRef>();
        this.resourceRef = new ArrayList<ResourceBus>();
    }

    public ResourceBusGeneric(String path, String name) {
        this.path = path;
        this.name = name;
        this.references = new ArrayList<ResourceRef>();
        this.resourceRef = new ArrayList<ResourceBus>();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setReferences(List<ResourceRef> references) {
        this.references = references;
    }
}
