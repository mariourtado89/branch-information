package cl.entel.tde.soa.integration.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Adapter {

    private String path;

    private String name;

    private Target target;

    private SystemProvider provider;

    private SystemApiProvider api;

    private SystemApiOperationProvider operation;

    private Map<String,Endpoint> endpoints;

    public Adapter() {
    }

    public Adapter(String path, String name, Target target) {
        this.path = path;
        this.name = name;
        this.target = target;
        this.endpoints = new HashMap<String, Endpoint>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTargetName(){
        return this.target.getTargetName();
    }

    public void setTargetName(String targetName){

    }

    public Map<String, Endpoint> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(Map<String, Endpoint> endpoints) {
        this.endpoints = endpoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adapter adapter = (Adapter) o;
        return Objects.equals(path, adapter.path);
    }

    @Override
    public int hashCode() {

        return Objects.hash(path);
    }

    public SystemProvider getProvider() {
        return provider;
    }

    public void setProvider(SystemProvider provider) {
        this.provider = provider;
    }

    public SystemApiProvider getApi() {
        return api;
    }

    public void setApi(SystemApiProvider api) {
        this.api = api;
    }

    public SystemApiOperationProvider getOperation() {
        return operation;
    }

    public void setOperation(SystemApiOperationProvider operation) {
        this.operation = operation;
    }
}
