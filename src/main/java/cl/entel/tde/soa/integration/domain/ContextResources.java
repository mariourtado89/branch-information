package cl.entel.tde.soa.integration.domain;

import cl.entel.tde.soa.integration.model.esb.ResourceBus;

import java.util.HashMap;
import java.util.Map;

public class ContextResources {

    public static Map<String, ResourceBus> resources = null;

    public static void put(String name, ResourceBus resource){
        if (resources == null){
            resources = new HashMap<String, ResourceBus>();
        }
        resources.put(name, resource);

    }
    public static ResourceBus get(String name){
        return resources.get(name);
    }
}
