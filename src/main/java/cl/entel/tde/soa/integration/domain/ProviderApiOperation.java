package cl.entel.tde.soa.integration.domain;

import java.util.HashMap;
import java.util.Map;

public class ProviderApiOperation {

    private String name;

    private String version;

    private String description;

    private Map<String, Endpoint> endpoints;

    public ProviderApiOperation(String name, String version, String description) {
        this.endpoints = new HashMap<String, Endpoint>();
        this.name = name;
        this.version = version;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Endpoint> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(Map<String, Endpoint> endpoints) {
        this.endpoints = endpoints;
    }
}
