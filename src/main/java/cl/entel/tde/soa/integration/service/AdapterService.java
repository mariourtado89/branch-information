package cl.entel.tde.soa.integration.service;

import cl.entel.tde.soa.integration.domain.Adapter;
import cl.entel.tde.soa.integration.domain.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class AdapterService {

    Logger logger = LoggerFactory.getLogger(AdapterService.class);

    private RestTemplate restTemplate;

    public AdapterService(){
        this.restTemplate = new RestTemplate();
    }

    public Adapter findAdapter(Target target){
        try{
        ResponseEntity<Adapter[]> response = restTemplate.getForEntity("http://172.28.41.22:10000/adapter/?provider=" + target.getTargetProvider() + "&api=" + target.getTargetApi() +"&operation=" + target.getTargetOperation() +"&version="+target.getTargetVersion(), Adapter[].class);
        if (response.getStatusCode().equals(HttpStatus.OK)){
            Adapter[] adapters = response.getBody();
            if(adapters.length == 1){
                return adapters[0];
            }
        }} catch (HttpClientErrorException e  ){
            logger.error(e.getMessage() + " -> " + target.getTargetName());
        }
        catch (Exception e ){
            logger.error(e.getMessage());
        }
        return null;
    }

}
