package cl.entel.tde.soa.integration.domain;

public class Target {

    private String targetProvider;

    private String targetApi;

    private String targetOperation;

    private String targetVersion;

    private String destination;

    private String targetName;

    private boolean valid;

    public Target() {
    }

    public Target(String targetProvider, String targetApi, String targetOperation, String targetVersion, String destination) {
        this.targetProvider = targetProvider;
        this.targetApi = targetApi;
        this.targetOperation = targetOperation;
        this.targetVersion = targetVersion;
        this.destination = destination;
    }

    public String getTargetProvider() {
        return targetProvider;
    }

    public void setTargetProvider(String targetProvider) {
        this.targetProvider = targetProvider;
    }

    public String getTargetApi() {
        return targetApi;
    }

    public void setTargetApi(String targetApi) {
        this.targetApi = targetApi;
    }

    public String getTargetOperation() {
        return targetOperation;
    }

    public void setTargetOperation(String targetOperation) {
        this.targetOperation = targetOperation;
    }

    public String getTargetVersion() {
        return targetVersion;
    }

    public void setTargetVersion(String targetVersion) {
        this.targetVersion = targetVersion;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public boolean isValid(){
        this.valid = !(getTargetProvider().equals("") || getTargetApi().equals("") || getTargetOperation().equals("") || getTargetVersion().equals(""));
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getTargetName(){
        this.targetName =  getTargetProvider()+"_"+getTargetApi()+"_"+getTargetOperation()+"_"+getTargetVersion();
        return this.targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }
}
