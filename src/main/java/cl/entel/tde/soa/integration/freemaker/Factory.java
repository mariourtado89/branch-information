package cl.entel.tde.soa.integration.freemaker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Map;

@Component
public class Factory {

    public Factory() {
    }

    public void generate(String relativePath, String templateName, String targetFile, Map<String, Object> map ){
        try{
            Configuration configuration = ConfigurationFactory.getInstance();
            Template template = configuration.getTemplate("/"+ relativePath + "/"+ templateName+ ".ftl");
            Writer fileWriter = new FileWriter(new File(targetFile));
            template.process(map, fileWriter);
            fileWriter.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
