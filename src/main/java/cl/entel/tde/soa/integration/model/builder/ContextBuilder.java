package cl.entel.tde.soa.integration.model.builder;

import cl.entel.tde.soa.integration.domain.*;
import cl.entel.tde.soa.integration.freemaker.AdapterFactory;
import cl.entel.tde.soa.integration.freemaker.ServiceFactory;
import cl.entel.tde.soa.integration.model.*;
import cl.entel.tde.soa.integration.model.esb.ProxyService;
import cl.entel.tde.soa.integration.model.esb.ResourceBus;
import cl.entel.tde.soa.integration.model.project.ServiceProject;
import cl.entel.tde.soa.integration.service.AdapterService;
import cl.entel.tde.soa.integration.service.ServiceService;
import cl.entel.tde.soa.integration.xml.xpath.XPathExecutor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ContextBuilder {

    Logger logger = LoggerFactory.getLogger(ContextBuilder.class);

    @Autowired
    private ModelBuilder modelBuilder;

    @Autowired
    private ServiceProject serviceProject;

    @Value( "${app.eusb.report.target.output}" )
    private String targetFile;

    @Value(("${app.eusb.report.target.output.adapter}"))
    private String targetAdapterFile;

    @Value(("${app.eusb.report.target.output.endpoints}"))
    private String endpintsFile;


    @Autowired
    private ServiceService serviceService;

    @Autowired
    private AdapterService adapterService;

    @Autowired
    private Context context;

    private ObjectMapper mapper;

    @Autowired
    private AdapterFactory adapterFactory;

    @Autowired
    private ServiceFactory serviceFactory;

    public ContextBuilder(){
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    @PostConstruct
    public void build() {
        try{
            DocumentBuilder builder = XPathExecutor.getDocumentBuilder();

            //File files = new File("/Users/mariopaulinourtadomedrano/Projects/entel/dev_arq10/01.FUENTES/01.OSB/01.WORKSPACE/list.txt");
            File files = new File("C:/Users/MURTADO/Projects/dev_arq10/01.FUENTES/01.OSB/01.WORKSPACE/out.txt");

            List<String> lines = FileUtils.readLines(files, "UTF-8");
            String path = lines.get(0).split("/ENTEL_ServiceBusApp/")[0] + "/ENTEL_ServiceBusApp/";

            ModelBuilder.projectPath = path;

            lines = this.filterBlackListFiles(lines);

            //List<String> ohs = lines.stream().filter(x-> x.startsWith("ES_")).filter(x-> x.contains("/OH/")|| x.contains("/PIF/")).collect(Collectors.toList());
            List<String> ohs = lines.stream().filter(x-> x.startsWith("ES_") || x.startsWith("DC_IE_")).collect(Collectors.toList());
            List<String> ras = lines.stream().filter(x-> x.startsWith("DC_RA")).collect(Collectors.toList());

            Map<String, Adapter> adapters = new HashMap<String, Adapter>();


            ohs.forEach(x-> {
                ResourceBus resource = modelBuilder.build(x);
                if(resource != null){
                    ContextResources.put(resource.getPath(), resource);
                }
            });
            ras.forEach(x-> {
                ResourceBus resource = modelBuilder.build(x);
                if(resource != null){
                    ContextResources.put(resource.getPath(), resource);
                    resource.findAdapter();
                    if (resource.getAdapter() != null){
                        Adapter adapter = resource.getAdapter();
                        Adapter adapterEndponit = adapterService.findAdapter(adapter.getTarget());
                        if(adapterEndponit != null){
                            adapter.setProvider(adapterEndponit.getProvider());
                            adapter.setApi(adapterEndponit.getApi());
                            adapter.setOperation(adapterEndponit.getOperation());
                            adapter.setEndpoints(adapterEndponit.getEndpoints());
                            adapters.put(adapter.getTargetName(), adapter);
                        }
                    }
                }
            });

            Set<String> keys = ContextResources.resources.keySet();
            for(String key : keys){
                ContextResources.get(key).bindResourceRefBus();
            }
            logger.info("Parse Lines in service project");
            for (String line: ohs){
                serviceProject.parse(line);
            }
            //serviceProject.getServices().forEach((x,y)-> y.getOperations().forEach((a,b)-> b.findServiceReference()));

            logger.info("BEGIN - Search codes service and operation");
            serviceProject.getServices().forEach((x,y)->{
                ServiceModel service = serviceService.findService(y.getName());
                if (service != null){
                    y.getOperations().forEach((a,b)->{
                        try{
                            service.getCapabilities().forEach((z)->{
                                if (z.getName().equals(b.getName())){
                                    b.setCode(z.getCode());
                                }
                            });
                        } catch ( Exception e ){
                            logger.error(e.getMessage() );
                        }
                    });
                    y.setCode(service.getCode());
                }
            });
            writeServiceProject();

            //serviceProject.getServices().forEach((k,v)->{v.getOperations().forEach((a,b)->{b.getAdapters().forEach(z-> adapters.put(z.getTargetName(), z) );});});
            mapper.writeValue(new File(targetAdapterFile), adapters);
            this.context.setServices(serviceProject.getServices());
            this.context.setAdapters(adapters);
            this.context.setServicesProject(serviceProject.getServices().keySet().stream().collect(Collectors.toList()));


            //Generate ES Script SQL
            serviceProject.getServices().forEach((x,y) ->{
                serviceFactory.generate(y);
            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void writeServiceProject(){
        try{

            mapper.writeValue(new File(targetFile), serviceProject.getServices());
        } catch(Exception e ){
            e.printStackTrace();
        }
    }

    public List<String> filterBlackListFiles(List<String> lines){
        return lines.stream()
                .filter(x-> !(new File(x).isDirectory()))
                .map(x-> x.split("/ENTEL_ServiceBusApp/")[1])
                .filter(x-> !x.startsWith("DC_SC"))
                .filter(x-> !x.startsWith("SR_Commons"))
                //.filter(x-> !x.startsWith("DC_IE"))
                .filter(x-> !x.endsWith(".xqy"))
                .filter(x-> !x.endsWith(".wsdl"))
                .filter(x-> !x.endsWith(".xsd"))
                .filter(x-> !x.endsWith(".wadl"))
                .filter(x-> !x.endsWith(".jca"))
                .filter(x-> !x.endsWith(".xsl"))
                .filter(x-> !x.endsWith(".XSLT"))
                .filter(x-> !x.endsWith(".LocationData"))
                .filter(x-> !x.endsWith(".Xquery"))
                .filter(x-> !x.endsWith(".ProxyService"))
                .filter(x-> !x.endsWith(".BusinessService"))
                .filter(x-> !x.endsWith(".mfl"))
                .filter(x-> !x.startsWith("System"))
                .filter(x-> !x.startsWith("SB_Dummy"))
                .filter(x-> !x.endsWith("servicebus.sboverview"))
                .filter(x-> !x.endsWith(".jpr"))
                .filter(x-> !x.endsWith(".xml"))
                .collect(Collectors.toList());

    }


    public void buildEndpint() {
        try{
            DocumentBuilder builder = XPathExecutor.getDocumentBuilder();

            //File files = new File("/Users/mariopaulinourtadomedrano/Projects/entel/dev_arq10/01.FUENTES/01.OSB/01.WORKSPACE/list.txt");
            File files = new File("C:/Users/MURTADO/Projects/dev_arq10/01.FUENTES/01.OSB/01.WORKSPACE/out.txt");

            List<String> lines = FileUtils.readLines(files, "UTF-8");
            String path = lines.get(0).split("/ENTEL_ServiceBusApp/")[0] + "/ENTEL_ServiceBusApp/";

            ModelBuilder.projectPath = path;

            lines = this.filterBlackListFiles(lines);

            List<String> ohs = lines.stream().filter(x-> x.startsWith("ES_")||x.contains("IE_")).filter(x-> x.endsWith(".proxy") || x.endsWith(".pipeline")).collect(Collectors.toList());


            Map<String, Adapter> adapters = new HashMap<String, Adapter>();


            ohs.forEach(x-> {
                ResourceBus resource = modelBuilder.build(x);
                if(resource != null){
                    ContextResources.put(resource.getPath(), resource);
                }
            });

            for (String line: ohs){
                serviceProject.parse(line);
            }

            Map<String, List<ResourceBus>> endpoints = new HashMap<String, List<ResourceBus>>();
            ContextResources.resources.forEach((x,y) -> {
                String project = x.split("/")[0];
                if (endpoints.get(project) == null){
                    endpoints.put(project, new ArrayList<ResourceBus>());
                }
                if(y.getType().equals(ResourceBusType.PROXY_SERVICE)){
                    ProxyService proxy = (ProxyService)y;
                    if(proxy.getTransport().equals("http")){
                        endpoints.get(project).add(proxy);
                    }
                }
            });

            System.out.println("Done!");
            System.out.println("Write It: " + endpintsFile);

            Map<String, ServiceOperationEndpoint> endpointsList = new HashMap<String,ServiceOperationEndpoint>();
            serviceProject.getServices().forEach((x,y) -> {
                ServiceOperationEndpoint soe = new ServiceOperationEndpoint(y);
                soe.setProxys(endpoints.get(x));
                endpointsList.put(x, soe);
            });

            Map<String, ServiceEndpointOperationDTO> serviceEndpointOperationDTOs = new HashMap<String,ServiceEndpointOperationDTO>();
            serviceProject.getServices().forEach((x,y)-> {
                ServiceEndpointOperationDTO dto = new ServiceEndpointOperationDTO(y.getName());
                List<String> operations = y.getOperations().keySet().stream().collect(Collectors.toList());
                List<String> endpointsService = endpointsList.get(x).getProxys().stream().map(z-> ((ProxyService)z).getUri()).collect(Collectors.toList());
                dto.setEndpoints(endpointsService);
                dto.setOperations(operations);
                serviceEndpointOperationDTOs.put(x, dto);
            });
            mapper.writeValue(new File(endpintsFile), serviceEndpointOperationDTOs);

            serviceEndpointOperationDTOs.forEach( (x,y) -> {
                System.out.println(y.getServiceName() + ";"+String.join(",", y.getOperations()) + ";" + String.join(";", y.getEndpoints().stream().sorted().collect(Collectors.toList())));
            });


        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
