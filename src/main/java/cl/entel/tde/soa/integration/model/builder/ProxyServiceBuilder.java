package cl.entel.tde.soa.integration.model.builder;

import cl.entel.tde.soa.integration.model.esb.ProxyService;
import cl.entel.tde.soa.integration.model.esb.ResourceRef;
import cl.entel.tde.soa.integration.xml.xpath.XPathExecutor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;
import java.util.ArrayList;
import java.util.List;


@Component
public class ProxyServiceBuilder {

    public ProxyService build(String filePath){
        try{

            Document document = XPathExecutor.getDocumentBuilder().parse(filePath);
            String name = filePath.split("/")[filePath.split("/").length-1];
            String uri = (String)XPathExecutor.execute(document, "/proxyServiceEntry/endpointConfig/URI/value", XPathConstants.STRING);
            String transport = (String)XPathExecutor.execute(document, "/proxyServiceEntry/endpointConfig/provider-id", XPathConstants.STRING);
            ProxyService resource = new ProxyService(filePath, name, uri, transport);
            List<ResourceRef> references = this.findReference(document);
            resource.setReferences(references);
            return resource;
        } catch (Exception e ){
            e.printStackTrace();
        }
        return null;
    }

    public List<ResourceRef> findReference(Document document){
        String resource = (String)XPathExecutor.execute(document, "/proxyServiceEntry/coreEntry/invoke/@ref", XPathConstants.STRING);
        String type = (String)XPathExecutor.execute(document, "/proxyServiceEntry/coreEntry/invoke/@type", XPathConstants.STRING);
        ResourceRef reference = new ResourceRef(resource, type, "");
        List<ResourceRef> references = new ArrayList<ResourceRef>();
        references.add(reference);
        return references;
    }

}

