package cl.entel.tde.soa.integration.domain;

public interface Provider {

    public String getCode();

    public void setCode(String code);
    public String getName();
    public void setName(String name);

    public String getDescription();
    public void setDescription(String description);

    public String getVersion();

    public void setVersion(String version);
}
