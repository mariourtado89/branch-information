package cl.entel.tde.soa.integration.service;

import cl.entel.tde.soa.integration.model.ServiceModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ServiceService {

    Logger logger = LoggerFactory.getLogger(ServiceService.class);

    private RestTemplate restTemplate;
    private ObjectMapper mapper;

    public ServiceService(){
        this.restTemplate = new RestTemplate();
        this.mapper = new ObjectMapper();
    }

    public ServiceModel findService(String name){
        ResponseEntity<ServiceModel[]> response = restTemplate.getForEntity("http://172.28.41.22:10000/service/?name=" + name, ServiceModel[].class);
        if (response.getStatusCode().equals(HttpStatus.OK)){
            ServiceModel[] services = response.getBody();
            if(services.length == 1){
                return services[0];
            }
            if(services.length > 1){
                try{
                    logger.debug("Too many results: " + mapper.writeValueAsString(services) );
                } catch (Exception e ){
                    logger.error("Error translate object to json in service with name " + name);
                }
            }
        }
        return null;
    }



}
