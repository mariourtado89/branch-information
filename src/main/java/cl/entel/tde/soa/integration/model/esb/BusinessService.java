package cl.entel.tde.soa.integration.model.esb;

import cl.entel.tde.soa.integration.domain.Adapter;
import cl.entel.tde.soa.integration.domain.ContextResources;
import cl.entel.tde.soa.integration.domain.ResourceBusType;
import java.util.List;

public class BusinessService extends ResourceBusGeneric implements ResourceBus {

    private String uri;

    private String transport;
    public BusinessService() {
        super();

    }
    public BusinessService(String path, String name, String uri, String transport) {
        super(path, name);
        this.uri = uri;
        this.transport = transport;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return ResourceBusType.BUSINESS_SERVICE;
    }
    public void setType(String type){

    }
    @Override
    public String getPath() {
        return this.path;
    }

    @Override
    public List<ResourceRef> getReference() {
        return this.references;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    @Override
    public List<ResourceBus> getResourcesRef() {
        return this.resourceRef;
    }

    @Override
    public Adapter findAdapter() {
        return null;
    }

    @Override
    public void bindResourceRefBus() {
        for(ResourceRef ref : getReference()){
            ResourceBus resource = ContextResources.get(ref.getReferenceResource());
            if (resource != null){
                this.getResourcesRef().add(resource);
            }
        }
    }

    @Override
    public Adapter getAdapter() {
        return null;
    }

    @Override
    public List<Adapter> collectAdapter(List<Adapter> adapters) {
        return adapters;
    }

    public void setAdapter(Adapter adapter){

    }
}
