package cl.entel.tde.soa.integration.model.esb;

import cl.entel.tde.soa.integration.builder.service.adapter.v1.ResourceAdapterReader;
import cl.entel.tde.soa.integration.domain.Adapter;
import cl.entel.tde.soa.integration.domain.ContextResources;
import cl.entel.tde.soa.integration.domain.ResourceBusType;
import cl.entel.tde.soa.integration.domain.Target;
import cl.entel.tde.soa.integration.model.builder.ModelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Pipeline extends ResourceBusGeneric implements ResourceBus {

    Logger logger = LoggerFactory.getLogger(Pipeline.class);

    private Adapter adapter = null;


    public Pipeline() {

    }

    public Pipeline(String path, String name) {
        super(path, name);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return ResourceBusType.PIPELINE;
    }
public void setType(String type){

}
    @Override
    public String getPath() {
        return path;
    }

    @Override
    public List<ResourceRef> getReference() {
        return references;
    }

    @Override
    public List<ResourceBus> getResourcesRef() {
        return this.resourceRef;
    }

    @Override
    public Adapter findAdapter() {
        ResourceAdapterReader reader = new ResourceAdapterReader();
        Target target = reader.read(ModelBuilder.projectPath + getPath());
        if(target.isValid()){
            this.adapter = new Adapter(getPath(), target.getTargetName(), target);
            logger.info("Adapter founded: " + target.getTargetName());
        }
        return this.adapter;
    }

    @Override
    public void bindResourceRefBus() {
        for(ResourceRef ref : getReference()){
            ResourceBus resource = ContextResources.get(ref.getReferenceResource());
            if (resource != null){
                this.getResourcesRef().add(resource);
            }
        }
    }
    @Override
    public Adapter getAdapter() {
        return this.adapter;
    }

    @Override
    public List<Adapter> collectAdapter(List<Adapter> adapters) {
        if (this.adapter != null){
            if (!adapters.contains(this.adapter)){
                adapters.add(this.adapter);
            }
        } else {
            for(ResourceBus resource : getResourcesRef()){
                if (resource != null){
                    resource.collectAdapter(adapters);
                }
            }
        }
        return adapters;
    }

    public void setAdapter(Adapter adapter){
        this.adapter = adapter;
    }
}
