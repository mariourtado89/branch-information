package cl.entel.tde.soa.integration.builder.service.adapter.v2;

import cl.entel.tde.soa.integration.domain.Target;
import cl.entel.tde.soa.integration.xml.xpath.XPathExecutor;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;

public class AdapterReader {

    public Target read(Document document){
        String tarProvider = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[@varName='TargetProvider']/expr/xqueryText/text()", XPathConstants.STRING);
        String tarApi = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[@varName='TargetAPI']/expr/xqueryText/text()", XPathConstants.STRING);
        String tarOperation = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[@varName='TargetOperation']/expr/xqueryText/text()", XPathConstants.STRING);
        String tarVersion = (String)XPathExecutor.execute(document, "//pipeline[@type='request']/stage/actions/assign[@varName='TargetVersion']/expr/xqueryText/text()", XPathConstants.STRING);
        String destination = (String)XPathExecutor.execute(document, "//route-node/actions/route/service/@ref", XPathConstants.STRING);
        Target target = new Target(tarProvider,tarApi,tarOperation,tarVersion, destination);
        return target;
    }
}
