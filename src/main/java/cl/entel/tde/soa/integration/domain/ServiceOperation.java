package cl.entel.tde.soa.integration.domain;

import cl.entel.tde.soa.integration.model.esb.BusinessService;
import cl.entel.tde.soa.integration.model.esb.ResourceBus;
import cl.entel.tde.soa.integration.model.esb.ResourceRef;
import java.util.ArrayList;
import java.util.List;

public class ServiceOperation {

    private String name;

    private String code;

    private List<ResourceBus> resources;

    private List<Adapter> adapters = null;

    private List<Service> services;

    public ServiceOperation() {
    }

    public ServiceOperation(String name, String code) {
        this.resources = new ArrayList<ResourceBus>();
        this.name = name;
        this.code = code;
        this.adapters = new ArrayList<Adapter>();
        this.services = new ArrayList<Service>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ResourceBus> getResources() {
        return resources;
    }

    public void setResources(List<ResourceBus> resources) {
        this.resources = resources;
    }

    public void addResources(ResourceBus resource){
        this.resources.add(resource);
    }

    public void setAdapters(List<Adapter> adapters) {
        this.adapters = adapters;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Adapter> getAdapters(){
        for (ResourceBus resource : getResources()){
            if(resource != null){
                resource.collectAdapter(this.adapters);
            }
        }
        return this.adapters;
    }

    public List<Service> getServices(){
        return this.services;
    }

    public void findServiceReference(){
        try{

            this.getResources().forEach(x-> x.getReference().forEach(y-> this.findServiceReference(y)));
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void findServiceReference(ResourceRef ref){
        if (ref.getReferenceResource().startsWith("ES_") && ref.getReferenceResource().endsWith(".proxy") && ref.getReferenceResource().contains("/PIF/")){
            String serviceName = ref.getRef().split("/")[0].split("_")[1];
            String serviceVersion = ref.getRef().split("/")[0].split("_")[2];
            String serviceOperation = ref.getOperation();
            this.services.add( new Service(serviceName, serviceVersion, serviceOperation));
        }
        if (ref.getReferenceResource().startsWith("ES_") && ref.getReferenceResource().endsWith(".bix")){
            BusinessService resourceBus = (BusinessService)ContextResources.get(ref.getReferenceResource());
            if (resourceBus.getUri().startsWith("http") && resourceBus.getUri().split("/").length > 5){
                String serviceName = resourceBus.getUri().split("/")[4];
                String serviceVersion = resourceBus.getUri().split("/")[5];
                String serviceOperation = ref.getOperation();
                this.services.add( new Service(serviceName, serviceVersion, serviceOperation));
            }
        }
    }
}
