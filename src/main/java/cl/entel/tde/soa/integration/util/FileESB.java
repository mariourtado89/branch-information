package cl.entel.tde.soa.integration.util;

public class FileESB {

    private String path;

    private String resource;

    public FileESB(String path, String resource) {
        this.path = path;
        this.resource = resource;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
}
