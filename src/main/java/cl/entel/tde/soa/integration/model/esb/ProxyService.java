package cl.entel.tde.soa.integration.model.esb;

import cl.entel.tde.soa.integration.domain.Adapter;
import cl.entel.tde.soa.integration.domain.ContextResources;
import cl.entel.tde.soa.integration.domain.ResourceBusType;

import java.util.List;

public class ProxyService extends ResourceBusGeneric implements ResourceBus {

    private String uri;

    private String transport;
    public ProxyService() {
        super();
    }
    public ProxyService(String path, String name, String uri, String transport) {
        super(path, name);
        this.uri = uri;
        this.transport = transport;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return ResourceBusType.PROXY_SERVICE;
    }

    @Override
    public String getPath() {
        return this.path;
    }

    @Override
    public List<ResourceRef> getReference() {
        return this.references;
    }
    public void setType(String type){

    }
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    @Override
    public List<ResourceBus> getResourcesRef() {
        return this.resourceRef;
    }

    @Override
    public Adapter findAdapter(){
        return null;
    }

    @Override
    public void bindResourceRefBus() {
        for(ResourceRef ref : getReference()){
            ResourceBus resource = ContextResources.get(ref.getReferenceResource());
            if (resource != null){
                this.getResourcesRef().add(resource);
            }
        }
    }

    @Override
    public Adapter getAdapter() {
        return null;
    }

    public void setAdapter(Adapter adapter){

    }

    @Override
    public List<Adapter> collectAdapter(List<Adapter> adapters) {
        for(ResourceBus resource : getResourcesRef()){
            resource.collectAdapter(adapters);
        }
        return adapters;
    }
}
