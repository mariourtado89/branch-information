package cl.entel.tde.soa.integration.model.builder;

import cl.entel.tde.soa.integration.model.esb.Pipeline;
import cl.entel.tde.soa.integration.model.esb.ResourceRef;
import cl.entel.tde.soa.integration.xml.xpath.XPathExecutor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import java.util.ArrayList;
import java.util.List;

@Component
public class PipelineBuilder {

    public Pipeline build(String filePath){
        Pipeline pipeline = null;
        try{

            Document document = XPathExecutor.getDocumentBuilder().parse(filePath);
            String name = filePath.split("/")[filePath.split("/").length-1];
            List<ResourceRef> serviceRefs = this.findReference(document);
            pipeline = new Pipeline(filePath, name);
            pipeline.setReferences(serviceRefs);
        } catch (Exception e ){
            e.printStackTrace();
        }
        return pipeline;
    }

    public List<ResourceRef> findReference(Document document){
        List<ResourceRef> serviceRefs = new ArrayList<ResourceRef>();
        NodeList reference = (NodeList)XPathExecutor.execute(document, "//service/@ref", XPathConstants.NODESET);
        List<String> references = new ArrayList<String>();
        for(int i = 0; i < reference.getLength() ; i ++){
            references.add(reference.item(i).getTextContent());

        }
        for (String ref: references) {
            String typeRef = (String)XPathExecutor.execute(document, "//service[@ref='"+ref+"']/@type", XPathConstants.STRING);
            String operation = (String)XPathExecutor.execute(document, "//service[@ref='"+ref+"']/../operation", XPathConstants.STRING);
            serviceRefs.add(new ResourceRef(ref, typeRef, operation));
        }
        return serviceRefs;
    }
}
