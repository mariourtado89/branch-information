package cl.entel.tde.soa.integration.domain;

import java.util.ArrayList;
import java.util.List;

public class ExternalProvider extends ServiceProvider {

    private List<ProviderApi> apis;

    public ExternalProvider(String code, String name, String description, String version) {
        super(code, name, description, version);
        this.apis = new ArrayList<ProviderApi>();
    }


    public List<ProviderApi> getApis() {
        return apis;
    }

    public void setApis(List<ProviderApi> apis) {
        this.apis = apis;
    }
}
