package cl.entel.tde.soa.integration.xml.xpath;


import org.w3c.dom.Document;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;

public class XPathExecutor {

    public static DocumentBuilder builder = null;

    public static XPath xpath = null;

    public static synchronized DocumentBuilder getDocumentBuilder() throws Exception{
        if (builder == null){
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
        }
        return builder;
    }

    public static synchronized XPath getXPath(){
        if (xpath == null){
            XPathFactory xPathfactory = XPathFactory.newInstance();
            xpath = xPathfactory.newXPath();
            //xpath.setNamespaceContext(new NamespaceResolver());
        }
        return xpath;
    }

    public static Object execute(Document document, String xPathExpression, QName qname){
        Object value = null;
        try {
            XPathExpression expr = getXPath().compile(xPathExpression);
            value = expr.evaluate(document,qname);
        } catch (XPathExpressionException e){
            e.printStackTrace();
        }
        return value;
    }


}
