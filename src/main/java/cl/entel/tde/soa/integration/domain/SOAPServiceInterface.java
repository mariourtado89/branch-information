package cl.entel.tde.soa.integration.domain;

import cl.entel.tde.soa.integration.domain.ServiceInterface;

public class SOAPServiceInterface  extends ServiceInterface {

    public String operation;

    public String soapAction;

    public String address;

    public SOAPServiceInterface(String type, String path, String method, String operation, String soapAction, String address) {
        super(type, path, method);
        this.operation = operation;
        this.soapAction = soapAction;
        this.address = address;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSoapAction() {
        return soapAction;
    }

    public void setSoapAction(String soapAction) {
        this.soapAction = soapAction;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean validateAddress(){
        return getAddress().endsWith(getPath());
    }
}
