package cl.entel.tde.soa.integration.domain;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Context {

    private Map<String,Service> services;

    private Map<String, Adapter> adapters;

    private List<String> adaptersNames;

    private List<String> servicesProject;

    public Context() {
        this.services = new HashMap<String, Service>();
        this.adapters= new HashMap<String, Adapter>();
        this.adaptersNames = new ArrayList<String>();
        this.servicesProject = new ArrayList<String>();
    }

    public Map<String, Service> getServices() {
        return services;
    }

    public void setServices(Map<String, Service> services) {
        this.services = services;
    }

    public Map<String, Adapter> getAdapters() {
        return adapters;
    }

    public void setAdapters(Map<String, Adapter> adapters) {
        this.adapters = adapters;
    }

    public List<String> getAdaptersNames() {
        return adaptersNames;
    }

    public void setAdaptersNames(List<String> adaptersNames) {
        this.adaptersNames = adaptersNames;
    }

    public List<String> getServicesProject() {
        return servicesProject;
    }

    public void setServicesProject(List<String> servicesProject) {
        this.servicesProject = servicesProject;
    }
}
