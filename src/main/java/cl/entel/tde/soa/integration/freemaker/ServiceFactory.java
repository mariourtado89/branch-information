package cl.entel.tde.soa.integration.freemaker;

import cl.entel.tde.soa.integration.domain.Adapter;
import cl.entel.tde.soa.integration.domain.Service;
import cl.entel.tde.soa.integration.domain.ServiceOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.security.auth.kerberos.ServicePermission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ServiceFactory {

    @Value("${app.eusb.report.target.sql}")
    private String targetDirectory;

    @Autowired
    private Factory factory;

    public ServiceFactory() {
    }

    public void build(List<Service> adapters){
        adapters.forEach(x-> {
            this.generate(x);
        });
    }

    public void generate(Service service){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("service", service);
            List<ServiceOperation> operations = new ArrayList<ServiceOperation>();
            service.getOperations().forEach((x,y)-> operations.add(y));
            map.put("operations", operations);
            factory.generate("freemaker/template", "service", targetDirectory + service.getName() + "_" + service.getVersion() + ".sql", map);
        }catch (Exception e ){
            e.printStackTrace();
        }

    }
}
