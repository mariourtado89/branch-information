package cl.entel.tde.soa.integration.domain;

import java.util.ArrayList;
import java.util.List;

public class ServiceEndpointOperationDTO {

    private String serviceName;

    private List<String> operations;

    private List<String> endpoints;

    public ServiceEndpointOperationDTO(String serviceName) {
        this.serviceName = serviceName;
        this.operations = new ArrayList<String>();
        this.endpoints = new ArrayList<String>();
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public List<String> getOperations() {
        return operations;
    }

    public void setOperations(List<String> operations) {
        this.operations = operations;
    }

    public List<String> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<String> endpoints) {
        this.endpoints = endpoints;
    }
}
