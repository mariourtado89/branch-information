package cl.entel.tde.soa.integration.domain;

public class ServiceInterface {

    private String type;

    private String path;

    private String method;

    public ServiceInterface(String type, String path, String method) {
        this.type = type;
        this.path = path;
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
