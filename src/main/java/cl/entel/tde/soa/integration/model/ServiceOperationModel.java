package cl.entel.tde.soa.integration.model;

public class ServiceOperationModel {

    private String name;

    private String code;

    public ServiceOperationModel() {
    }

    public ServiceOperationModel(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
